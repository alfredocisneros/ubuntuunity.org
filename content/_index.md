---
# banner
banner:
  title: "Unity, once again."
  button: "Download"
  button_link: "download/"
  image: "images/laptop-ubuntuunity.png"


# brands
brands_carousel:
  enable: false
  brand_images:
  - " "


# features
features:
  enable: true
  title: "Features"
  description: "Unity7 ain't ded ;)"
  features_blocks:
  - icon: "las la-question"
    title: "Sticking to 16.04?"
    content: "Are you staying with Ubuntu 16.04 Xenial Xerus only for Unity7? This remix will let you upgrade peacefully, without having to worry about losing Unity7."
  - icon: "las la-gift"
    title: "Unity7 Goodies"
    content: "This remix comes with the old, loved Unity7 goodies like HUD, Global Menu, so that you can continue using the features of Unity7 which you had used on the previous versions of Ubuntu before 17.10 Artful Aardvark."
  - icon: "las la-clock"
    title: "Unity7 Modernized"
    content: "We have ported Yaru to Unity7, replacing the old Ambiance theme. However, if you want to stick to the Ambiance theme, you can just switch to it by using **unity-tweak-tool**."


# intro_video
intro_video:   
  enable: true
  subtitle: "Linux For Everyone"
  title: "A review of Ubuntu Unity 20.04"
  description: "A video about the first release of Ubuntu Unity (released in May 2020)."
  video_url: "https://www.youtube.com/embed/VAbD99Zk2Jc"
  video_thumbnail: "images/l4e.jpg"

# testimonials
testimonials:   
  enable: true
  title: "<br><br>Testimonials"
  image_left: "none"
  image_right: "none"
  
  testimonials_quotes:
  - quote: "If you’re yearning for the good ole’ Unity and Compiz days, I bring awesome tidings: someone’s shining a new spotlight on them, and the stage underneath is a brand new Linux distribution called Ubuntu Unity Remix 20.04."
    name: "Jason Evanghelo"
    designation: "Forbes"
    image: "none"

  - quote: "I took Ubuntu Unity Remix 20.04 for a spin and it brought back good old memories for me. The spin looks great and works like a charm."
    name: "Marius Nestor"
    designation: "9to5Linux"
    image: "none"

  - quote: "Unity is back as is your ticket out of a world of inefficient desktop interfaces that made using a laptop a less-than-ideal proposition."
    name: "Jack Wallen"
    designation: "TechRepublic"
    image: "none"

  - quote: "Yes, this was the old Unity I used to know and love, but somehow it felt fresher. As I worked to regain muscle memory over the key-bindings (GNOME really can take over the way you control your system XD), the experience was smooth, graceful, and fun in a way that is unique to the Unity experience."
    name: "Eric Londo"
    designation: "Linux++, Destination Linux Network"
    image: "none"

---
