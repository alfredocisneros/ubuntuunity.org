---
title: "Team"
layout: "about"
draft: false

# our team
project_lead:
  enable: true
  subtitle: "<br><br>Our lead developer"
  title: "Project Lead"
  name: "Rudra Saraswat (`@RudraSaraswat1` on Twitter)"
  image: "images/about/team/rs2009.gif"
  designation: "**Creator, Project Lead, dev & Ubuntu Member**"

# our team
our_team:
  enable: true
  subtitle: "<br><br>Our members"
  title: "Team"
  team:
  - name: "Allan (`@alera_on`)"
    image: "images/about/team/allan.png"
    designation: "Designer"
  - name: "Maik (`@Maik_aD`)"
    image: "images/about/team/maik.jpg"
    designation: "Telegram, Matrix, Discord mod, Ubuntu Member"
  - name: "Tobiyo (`@fuseteam`)"
    image: "images/about/team/fuseteam.jpg"
    designation: "Telegram, Matrix & Discord mod, involved in Ubuntu Touch"
  - name: "Muqtadir (`@muqtxdir`)"
    image: "images/about/team/muqtxdir.jpg"
    designation: "Designer"

---
