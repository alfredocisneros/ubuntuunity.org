---
title: "Press"
layout: "about"
draft: false

# what_we_do
what_we_do:
  enable: true
  title: "<br>Press Coverage"
  block:
  - title: "Forbes"
    content: "\"If you’re yearning for the good ole’ Unity and Compiz days, I bring awesome tidings: someone's shining a new spotlight on them, and the stage underneath is a brand new Linux distribution called Ubuntu Unity Remix 20.04.\" **- Jason Evanghelo**

    <br><br>[**Read more >>**](https://www.forbes.com/sites/jasonevangelho/2020/05/12/a-surprising-new-remix-of-ubuntu-2004-revives-the-unity-desktop/?sh=1468558b275b)"

  - title: "TechRepublic"
    content: "\"If you've been waiting for the return of the Unity desktop, your wait is over. Ubuntu Unity is a fresh take on the once-defunct interface.\" **- Jack Wallen**

    <br><br>[**Read more >>**](https://www.techrepublic.com/article/ubuntu-unity-brings-back-one-of-the-most-efficient-desktops-ever-created/)"

  - title: "9to5Linux"
    content: "\"The Ubuntu Unity team released today Ubuntu Unity 21.10 as the latest version of their unofficial Ubuntu flavor featuring the good old Unity desktop environment.\" **- Marcus Nestor**

    <br><br>[**Read more >>**](https://9to5linux.com/ubuntu-unity-21-10-released-to-keep-the-unity-desktop-alive-in-2021)"

  - title: "Wikipedia"
    content: "\"Ubuntu Unity is a Linux distribution based on Ubuntu, using the Unity interface in place of Ubuntu's GNOME Shell. The first release was 20.04 LTS on 7 May 2020. Prior to the initial release it had the working names of Unubuntu and Ubuntu Unity Remix.\"

    <br><br>[**Read more >>**](https://en.wikipedia.org/wiki/Ubuntu_Unity)"

---
