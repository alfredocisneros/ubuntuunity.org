---
title: "Download"
layout: "single"
draft: false
---

<h3 style="text-align: center;">Ubuntu Unity 22.04 (the current LTS and stable version)</h2>

<p style="text-align: center;">
    <a href="http://linux.darkpenguin.net/distros/ubuntu-unity/22.04" class="btn btn-primary" style="color: white; text-decoration: none;">Download from server</a>
    <a href="https://fosstorrents.com/distributions/ubuntu-unity/" class="btn btn-primary" style="color: white; text-decoration: none; margin: 15px;">Download torrent</a>
</p>

<h3 style="text-align: center;">Ubuntu Unity 21.10 (the old stable release)</h2>

<p style="text-align: center;">
    <a href="http://linux.darkpenguin.net/distros/ubuntu-unity/21.10" class="btn btn-primary" style="color: white; text-decoration: none;">Download from server</a>
    <a href="https://fosstorrents.com/distributions/ubuntu-unity/" class="btn btn-primary" style="color: white; text-decoration: none; margin: 15px;">Download torrent</a>
</p>

<h3 style="text-align: center;">Ubuntu Unity 20.04.4 (the old LTS release)</h2>

<p style="text-align: center;">
    <a href="http://linux.darkpenguin.net/distros/ubuntu-unity/20.04.4" class="btn btn-primary" style="color: white; text-decoration: none;">Download from server</a>
    <a href="https://fosstorrents.com/distributions/ubuntu-unity/" class="btn btn-primary" style="color: white; text-decoration: none; margin: 15px;">Download torrent</a>
</p>

<h3 style="text-align: center;">Other downloads (including testing releases)</h2>

<p style="text-align: center;">
    <a href="http://linux.darkpenguin.net/distros/ubuntu-unity/" class="btn btn-primary" style="color: white; text-decoration: none;">Download from server</a>
    <a href="https://fosstorrents.com/distributions/ubuntu-unity/" class="btn btn-primary" style="color: white; text-decoration: none; margin: 15px;">Download torrent</a>
</p>