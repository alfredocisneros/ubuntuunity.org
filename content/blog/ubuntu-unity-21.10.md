---
date: "2021-10-14"
title: "Ubuntu Unity 21.10 released"
image: "images/blog/ubuntu-unity-21.10.png"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
draft: false
---

Ubuntu Unity 21.10 has now been released! You can download it from https://ubuntuunity.org/download/.

Unity7 has some major changes in 21.10, including the new and updated indicators and the migration of the glib-2.0 schemas to `gsettings-ubuntu-schemas`.

We now have a new simplified logo designed by Muqtadir and Allan Carvalho. We are also slowly migrating our website to GitLab, since our old website and server could not handle
the heavy traffic. We have a new Ubiquity Plymouth splash screen and a number of new wallpapers.

We are now using the new Firefox snap. Speaking of snaps, we recently released the first beta of [lol](https://repo.lolsnap.org/lol-snap/lol), an open-source
alternative to the Snap Store, although it won't be shipping in 21.10. We are thankful to fosshost for providing a VM for hosting the lol snap store and Discourse
forum at https://forum.lolsnap.org.

We have published the ISO builder for Ubuntu Unity as the [Ubuntu Remixes project](https://gitlab.com/ubuntu-unity/ubuntu-remixes) for anyone to build new Ubuntu remixes.
Ubuntu Cinnamon Remix has switched to it and is using it for their 21.10 release. It's really simple to build with and we built an Ubuntu remix with it live on UbuCon Asia
within 15 minutes. You shouldn't face any issues with the GPG key files for a PPA unlike `lb` when building your own flavor of Ubuntu ;) For support or queries related to
the builder, please join us on Telegram at <a href="https://t.me/ubunturemixes">t.me/ubunturemixes</a>.

Also, do join the Ubuntu Release Party today at https://youtu.be/8ZTI7kdPFLc!