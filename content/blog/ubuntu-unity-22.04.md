---
date: "2022-04-21"
title: "Ubuntu Unity 22.04 released"
image: "images/blog/ubuntu-unity-22.04.png"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
draft: false
---

Ubuntu Unity 22.04 has now been released (and is the successor to 20.04 LTS)! You can download it from https://ubuntuunity.org/download/.

We have added flatpak and the Flathub repo by default to 22.04. The `gtk3-nocsd` issue has been fixed in 22.04.

The following GNOME apps are being replaced with alternatives which fit in with Unity's UI better:

* Document Viewer - with Atril
* Text Editor - with Pluma
* Video Player - with VLC
* Image Viewer - with EOM
* System Monitor - with MATE System Monitor

The BIOS and UEFI ISOs are no longer split, and you can use the same ISO for both. The Firefox snap is being used by default.

We'll be releasing Ubuntu Unity 22.04.1 in August/September.

For support or queries, please join us on Telegram at [t.me/ubuntuunitydiscuss](https://t.me/ubuntuunitydiscuss).

Also, do comment on https://twitter.com/ubuntu/status/1516807006161674251?s=21&t=QYAQRQQydAytZJqn1OW1CA and suggest a song to add to "the FIRST EVER Ubuntu release playlist for 22.04 LTS"!