---
date: "2022-02-24"
title: "Ubuntu Unity 20.04.4 and 22.04 \"Jammy Jellyfish\" Feature Freeze"
image: "images/blog/ubuntu-unity-20.04.4.png"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
draft: false
---

We are delighted to announce that we are releasing Ubuntu Unity 20.04.4 LTS, as well as 22.04's feature freeze ISO. You can download the 22.04 ISO and 20.04.4 ISO from https://ubuntuunity.org/download/.

We have added flatpak by default to both 20.04.4 and 22.04. The `gtk3-nocsd` issue has been fixed in 22.04. The `yaru-theme-unity` package has been included by default in 20.04.4 and 22.04 to fix issues with some snaps. The following GNOME apps are being replaced with alternatives which fit in with Unity's UI better (in both 20.04.4 and 22.04):

* Document Viewer - with **Atril**
* Text Editor - with **Pluma**
* Video Player - with **VLC**
* Image Viewer - with **EOM**
* System Monitor - with **MATE System Monitor**

You can create a 22.04 VM in Boxes (free/libre), VirtualBox (free/libre) or VMware Player (proprietary) for testing the new 22.04 ISO. You might need additional hardware, since a few packages
cannot be tested in a virtual machine. If you find a bug, run: `ubuntu-bug <PKGNAME>`. You can also chat live with us on Telegram at [https://t.me/ubuntuunitydiscuss](https://t.me/ubuntuunitydiscuss).

If you’d like to read up more on being a tester for 22.04, here are some useful links.

* https://www.youtube.com/watch?v=hXLiqjOkSmg
* https://www.youtube.com/watch?v=lOCWwLwN7xE
* https://www.youtube.com/watch?v=4Ou1-zRSo-8
* https://wiki.ubuntu.com/QATeam/Overview/NewTesters
* https://wiki.ubuntu.com/Testing/ISO/Walkthrough
* https://wiki.ubuntu.com/QATeam/DevelopmentSetup
* https://launchpad.net/~ubuntu-testing