---
date: "2021-10-07"
title: "Ubuntu Unity 21.10 RC has been released"
image: "images/blog/ubuntu-unity-21.10-rc.png"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
draft: false
---

The release candidate ISO for Ubuntu Unity 21.10 has now been released. We are also slowly migrating our website and blog to a new server! You can get the ISO from the DarkPenguin source at
[https://ubuntuunity.org/page/downloads](/page/downloads).

Unity7 is going to have some REALLY major changes in 21.10, including the updated indicators and the migration of the glib-2.0 schemas. We are now using the new Firefox snap. Speaking of snaps,
we have also been busy working on https://repo.lolsnap.org/lol-snap/lol, an open-source alternative to the Snap Store, although it won't be shipping in 21.10. We are thankful to fosshost for
providing a VM for hosting the lol snap store and Discourse forum at https://forum.lolsnap.org.

If you have already installed the beta, you can just update your machine to the RC with the Software Updater GUI or by running `sudo apt update && sudo apt upgrade` in the terminal.

### The Ubuntu Community Twitter account

A new Twitter account for Ubuntu was created recently by the Ubuntu community (Canonical isn't involved in it). Its Twitter handle is [@ubuntudesktop](https://twitter.com/ubuntudesktop).
Its primary focus is on the desktop, unlike the Ubuntu account managed by Canonical, which is mostly focused on Canonical's products such as WSL2 and the cloud.

**Happy testing!**