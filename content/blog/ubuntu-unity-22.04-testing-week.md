---
date: "2021-12-23"
title: "Ubuntu Unity 22.04 \"Jammy Jellyfish\" Testing Week"
image: "images/blog/ubuntu-unity-spinner.gif"
author_info: 
  name: "Rudra Saraswat"
  image: "images/about/team/rs2009.gif"
draft: false
---

We are delighted to announce that we are participating in the 'Ubuntu Testing Week' for the 22.04 release cycle, from December 23rd to December 29th with other Ubuntu flavo(u)rs
and remixes. The testing week ISO has been uploaded to the server provided to us by DarkPenguin. The testing session will happen for 7 days starting from today, but you are
welcome to continue testing even after the conclusion of the Ubuntu Testing Week on December 29th. Please use the #UbuntuTestingWeek hashtag on social media to spread the word
about the event and to encourage others to participate as well. You can download the 22.04 ISO from https://ubuntuunity.org/download/.

You can create a testing VM for 22.04 in Boxes (free/libre), VirtualBox (free/libre) or VMware Player (proprietary). You might need additional hardware, since a few packages
cannot be tested in a virtual machine. If you find a bug, run: `ubuntu-bug <PKGNAME>`. You can also chat live with us on Telegram at [https://t.me/ubuntuunitydiscuss](https://t.me/ubuntuunitydiscuss).

We have added flatpak by default to both 20.04.4 and 22.04.

The following GNOME apps are being replaced with alternatives which fit in Unity's UI better (in both 20.04.4 and 22.04):

* Document Viewer - with Atril
* Text Editor - with Pluma
* Video Player - with VLC
* Image Viewer - with EOM
* System Monitor - with MATE System Monitor

If you’d like to read up more on being a tester for 22.04, here are some useful links.

* https://www.youtube.com/watch?v=hXLiqjOkSmg
* https://www.youtube.com/watch?v=lOCWwLwN7xE
* https://www.youtube.com/watch?v=4Ou1-zRSo-8 https://wiki.ubuntu.com/QATeam/Overview/NewTesters
* https://wiki.ubuntu.com/Testing/ISO/Walkthrough
* https://wiki.ubuntu.com/QATeam/DevelopmentSetup
* https://launchpad.net/~ubuntu-testing