---
title: "Chats"
layout: "single"
draft: false
---

<h2 style="text-align: center;">
    Please keep in mind that regardless of what chat service you use for our chatroom, you will still able to talk to anyone from any chat service in our chatroom.
</h2>

<h3 style="text-align: center;">Telegram</h2>

<p style="text-align: center;">
    Recommended chat service. May be useful if you already have a Telegram account. Service is not completely free and open source.
</p>

<p style="text-align: center;">
    <a href="https://t.me/ubuntuunitydiscuss" class="btn btn-primary" style="color: white; text-decoration: none;">Join</a>
</p>

<h3 style="text-align: center;">Matrix</h3>

<p style="text-align: center;">
    Decentralized and open communication with clients available for both desktop and mobile. May be useful if you already have a Matrix account. Service is completely free and open source.
</p>

<p style="text-align: center;">
    <a href="https://matrix.to/#/+ubuntuunity:matrix.org" class="btn btn-primary" style="color: white; text-decoration: none;">Join</a>
</p>

<h3 style="text-align: center;">Discord</h3>

<p style="text-align: center;">
    Alternative chat service. May be useful if you already have a Discord account. Service is not free and open source.

<p style="text-align: center;">
    <a href="https://discord.com/invite/6c6yE7zQ" class="btn btn-primary" style="color: white; text-decoration: none;">Join</a>
</p>
